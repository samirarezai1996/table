import React, {Component} from 'react';
import data from 'assets/data/datatest.json';
import Search from "components/table/search";
import {connect} from "react-redux";
import {listStar} from "redux/pages/action";


class Index extends Component {
    state = {
        initialData: [],
        data: [],
        filter: '',
        queryParam: {
            sort: 'name',
            filter: {
                value: '',
                name: 'name'
            }
        }
    }

    componentDidMount() {

        if (this.props.location?.search) {
            let queryParam = (this.props.location?.search) &&
                JSON.parse(decodeURIComponent(this.props.location?.search)?.replace('?', ''));
            this.setState({
                queryParam
            })
        }


        this.setState({
            initialData: data,
            data: data
        }, () => {
            this.getFilter((this.state.queryParam?.filter?.value), this.state.queryParam?.filter?.name || 'name')
        })

    }

    Sort = (item = 'name') => {
        this.props.history.push({
            pathname: '/',
            search: encodeURIComponent(JSON.stringify({
                ...this.state.queryParam,
                sort: item
            }))
        })

        let t = this.state.data.sort((a, b) => {
            let x = a?.[item];
            let y = b?.[item];
            if (x < y) {
                return -1;
            }
            if (x > y) {
                return 1;
            }
            return 0;
        })
        this.setState({
            data: t,
            queryParam: {
                ...this.state.queryParam,
                sort: item
            }
        })
    }
    getFilter = (e = '', name = '') => {

        this.props.history.push({
            pathname: '/',
            search: encodeURIComponent(JSON.stringify({
                ...this.state.queryParam,
                filter: {value: e, name: name}
            }))
        })
        let t = this.state.initialData;
        let list = t.filter(t => {
            return ((t?.[name]?.toUpperCase()?.indexOf(e) > -1) || t?.[name]?.toLowerCase()?.indexOf(e) > -1)
        });

        this.setState({
            data: list,
            queryParam: {
                ...this.state.queryParam,
                filter: {value: e, name: name}
            }
        }, () => this.Sort(this.state.queryParam?.sort))
    }

    render() {
        return (
            <>
                <div className="main">
                    <div className="box-input">
                        <Search getFilter={this.getFilter} name='name' default={this.state.queryParam?.filter}
                                label="نام تغییر دهنده"/>
                        <Search getFilter={this.getFilter} name='date' default={this.state.queryParam?.filter}
                                label="تاریخ"/>
                        <Search getFilter={this.getFilter} name='title' default={this.state.queryParam?.filter}
                                label="نام آگهی"/>
                        <Search getFilter={this.getFilter} name='field' default={this.state.queryParam?.filter}
                                label="فیلد"/>
                    </div>

                    <table>
                        <thead>
                        <tr>
                            <th>
                                <button onClick={() => this.Sort('name')}>نام تغییر دهنده</button>
                            </th>
                            <th>
                                <button onClick={() => this.Sort('date')}>تاریخ</button>
                            </th>
                            <th>
                                <button onClick={() => this.Sort('title')}>نام آگهی</button>
                            </th>
                            <th>
                                <button onClick={() => this.Sort('field')}>فیلد</button>
                            </th>
                            <th>
                                <button onClick={() => this.Sort('old_value')}>مقدار قدیمی</button>
                            </th>
                            <th>
                                <button onClick={() => this.Sort('new_value')}>مقدار جدید</button>
                            </th>
                            <th>
                                <button>ستاره دار</button>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.state.data.map((r, i) => <tr key={i}>
                            <td>{r?.name}</td>
                            <td>{r?.date}</td>
                            <td>{r?.title}</td>
                            <td>{r?.field}</td>
                            <td>{r?.old_value}</td>
                            <td>{r?.new_value}</td>
                            <td>
                                <button
                                    className={`${this.props.infoListStar.some(t => t== r?.id) && 'text-pink'}`}
                                    onClick={() => this.props.listStar(r?.id)}>★
                                </button>
                            </td>
                        </tr>)}
                        </tbody>


                    </table>
                </div>
            </>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        listStar: (...data) => dispatch(listStar(...data)),
    }
};

const mapStateToProps = (store) => {
    return ({
        infoListStar: store.infoListStar

    })
}
export default connect(mapStateToProps, mapDispatchToProps)(Index)