import React, {Component} from 'react';

class Search extends Component {
    render() {
        return (
            <>
                <div className="item-input">
                    <p>{this.props.label}</p>
                    <input className="input-search"
                           type="text"
                           value={(this.props.default?.name===this.props.name) ? this.props.default?.value : ''}
                           onChange={(e) => this.props.getFilter(e.target.value, this.props.name)}
                    />
                </div>
            </>
        );
    }
}

export default Search;