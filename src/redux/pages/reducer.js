import {STAR} from 'redux/pages/type';

export function infoListStar(state = [], action) {
    switch (action.type) {
        case STAR:
            return [...state,action.data];
        default:
            return state;
    }
}
